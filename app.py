import os
import subprocess

from flask import Flask, request, render_template, make_response, Response, jsonify
from werkzeug.utils import secure_filename

from PIL import Image, ImageFilter
import hashlib

UPLOAD_FOLDER = 'uploaded_files/'
ALLOWED_EXTENSIONS = {'pdf',
                      'png',
                      'jpg',
                      'JPG',
                      'jpeg',
                      'gif',
                      'tif',
                      'tiff'}

app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1024 * 1024  # else, return 413

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        #custom = request.form['CustomField']
        if file:
            hash = hashfile(file, hashlib.sha256())[:16]
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], hash))
            return jsonify(fileid=hash, error="")
    return render_template('index.html')

def hashfile(afile, hasher, blocksize=65536):
    buf = afile.read(blocksize)
    while len(buf) > 0:
        hasher.update(buf)
        buf = afile.read(blocksize)
    return "".join("{0:x}".format(ord(c)) for c in hasher.digest())

def cleanup(*args):
    for i in args:
        os.remove(i)

if __name__ == "__main__":
    app.debug = True 
    app.run(port=8012, host='0.0.0.0')
