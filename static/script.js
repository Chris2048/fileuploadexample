//MyApp = new Backbone.Marionette.Application();
// 
//MyApp.addRegions({
//  mainRegion: "#content"
//});

// upload JPEG files
function UploadFile(file, postUrl, progressElement, successCallback, failCallback) {
	var xhr = new XMLHttpRequest();
	if (xhr.upload) {
	// progress bar
    if (progressElement){
    		// create progress bar
		var progress = progressElement.appendChild(document.createElement("p"));
		progress.appendChild(document.createTextNode("upload " + file.name));
        
		xhr.upload.addEventListener("progress", function(e) {
			var pc = parseInt(100 - (e.loaded / e.total * 100));
			progress.style.backgroundPosition = pc + "% 0";
		}, false);
// file received/failed
		xhr.onreadystatechange = function(e) {
			if (xhr.readyState == 4) {
				progress.className = (xhr.status == 200 ? "success" : "failure");
			}
		};
        };
        
        
    var returnData = 
    function(f) {
    return function(oEvent) {
    return f(xhr.response);
    };
    };
    
        
    // start upload
    xhr.addEventListener("load", returnData(successCallback), false);
    xhr.addEventListener("error", returnData(failCallback), false);
    //xhr.addEventListener("abort", callback, false);

        var formData = new FormData();
        formData.append('file', file);
        
        xhr.open("POST", postUrl, true);
		xhr.send(formData);
	}
}

function sendForm() {
    var oOutput = document.getElementById("output");
        
    var fileInput = document.getElementById('the-file');
    var file = fileInput.files[0];

    var returnData = function(oResponse) {
        console.log(oResponse);
        return oResponse;
    };
    UploadFile(file, "/", document.getElementById("progress"), returnData, returnData)
};
